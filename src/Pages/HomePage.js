import React from 'react'
import styled from 'styled-components'
import Particle from '../components/Particles'
import FacebookIcon from '@material-ui/icons/Facebook'
import GithubIcon from '@material-ui/icons/GitHub'
import LinkedInIcon from '@material-ui/icons/LinkedIn';
import avatar from '../images/welcome.svg'

const HomePage = () => {
    return (
        <HomePageStyled>
            <div className="p-particles-js">
                <Particle />
            </div>
            <div className="typography">
                <div className="welcome-pic">
                    <img src={avatar} alt="welcome" />
                </div>
                <h1>Hi I'm <span>Krishna Mohan</span></h1>
                <p>I am a designer / Front end developer, Web developer , Building Single page applications, Living in Stockholm, SWEDEN</p>
                <div className="icons">
                    <a href="https://www.facebook.com/krishnamohan.paruchuri" className="icon i-facebook">
                        <FacebookIcon />
                    </a>
                    <a href="https://github.com/krishnamohanparuchuri" className="icon i-github">
                        <GithubIcon />
                    </a>
                    <a href="https://www.linkedin.com/in/krishna-mohan-paruchuri-39b69021/" className="icon i-linkedin">
                        <LinkedInIcon />
                    </a>
                </div>
            </div>
        </HomePageStyled>
    )
}

const HomePageStyled = styled.header`
width:100%;
height:100vh;
position:relative;

  .p-particles-js{
     position:absolute;
     top:0;
     left:0;
     width:100vw;
 }
  .typography{
     position:absolute;
     top:40%;
     left:50%;
     transform: translate(-50%,-50%);
     text-align:center;
     width:65%;
     .welcome-pic{
         img{
             width:8rem;
             height:8rem;
         }
     }
     .icons{
         display:flex;
         justify-content:center;
         margin-top:1rem;
        .icon{
            border: 2px solid var(--border-color);
            display:flex;
            align-items:center;
            justify-content:center;
            border-radius:50%;
            transition: all .4s ease-in-out;
            cursor: pointer;
            &:hover{
                border:2px solid var(--primary-color);
                color:var(--primary-color);
            }
            &:not(:last-child){
                margin-right:1rem;
            }
            svg{
                margin:.5rem;
            }
        }
     }

 }

`;
export default HomePage
