import React from 'react'
import styled from 'styled-components'
import avatar from '../images/contact.svg'

const Contact = () => {
    return (
        <ContactStyled>
            <div className="conatct-container">
                <img src={avatar} alt="contact" />

            </div>

        </ContactStyled>
    )
}

const ContactStyled = styled.div`
 img{
     margin-left:16.3rem;
     width:20rem;
     height:15rem;
 }
`;

export default Contact
