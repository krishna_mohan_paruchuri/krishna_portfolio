import React from 'react'
import styled from 'styled-components'
import { MainLayout } from '../styles/Layout'
import Title from '../components/Title'
import ImageSection from '../components/ImageSection'
import ServiceSection from '../components/ServiceSection'
import ReviewsSection from '../components/ReviewsSection'

const AboutPage = () => {
    return (
        <MainLayout>
            <AboutStyled>
                <Title title={'About Me'} span={'About Me'} />
                <ImageSection />
            </AboutStyled>
            <ServiceSection />
            <ReviewsSection />
        </MainLayout>
    )
}

const AboutStyled = styled.section`
`;

export default AboutPage
