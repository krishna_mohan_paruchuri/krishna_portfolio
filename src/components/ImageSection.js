import React from 'react'
import styled from 'styled-components'
import resume from '../images/about_me.svg'
import PrimaryButton from './Primarybutton'

const ImageSection = () => {
    return (
        <ImageSectionStyled>
            <div className="left-content">
                <img src={resume} alt="resume" />
            </div>
            <div className="right-content">
                <h4>
                    I am <span>krishna Mohan</span>
                </h4>
                <p className="paragraphy">
                    Courteous and enthusiastic, I am interested in IT and everything in its orbit. I recently began to be fascinated by web programming, e.g. developing apps and building websites.
                </p>
                <div className="about-info">
                    <div className="info-title">
                        <p>Full Name</p>
                        <p>Age</p>
                        <p>Nationality</p>
                        <p>Languages</p>
                        <p>Location</p>
                        <p>Service</p>
                    </div>
                    <div className="info">
                        <p>: Krishna Mohan</p>
                        <p>: 36</p>
                        <p>: Swedish</p>
                        <p>: English / Swedish / Telugu / Hindi / Tamil</p>
                        <p>: Stockholm, Märsta</p>
                        <p>: Free lancer</p>
                    </div>
                </div>
                <PrimaryButton title={'Download CV'} />
            </div>

        </ImageSectionStyled>
    )
}
const ImageSectionStyled = styled.div`
display:flex;
margin-top:3rem;
.left-content{
    width:100%;
    img{
        width:20rem;
        height:50vh;
    }
}

.right-content{
    margin-left:2rem;
    h4{
        font-size:2rem;
        color: var(--white-color);
        span{
            font-size:2rem;
        }
    }
    .paragraphy{
        padding:.8rem 0;
    }
    .about-info{
       display:flex;
       padding-bottom:1rem;
       .info-title{
           padding-right:2rem;
       }
       .info-title,.info{
        p{
            padding: .2rem 0;
        }
       } 
    }
}

`;

export default ImageSection
