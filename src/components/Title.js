import React from 'react'
import styled from 'styled-components'

const Title = ({ title, span }) => {
    return (
        <TitleStyled>
            <h2>{title}<span>{span}</span></h2>

        </TitleStyled>
    )
}
const TitleStyled = styled.div`
   position:relative;
   h2{
       color:var(--white-color);
       font-size:2rem;
       font-weight:600;
       text-transform:uppercase;
       position:relative;
       z-index:2;
       padding-bottom:.3rem;
       &::before{
           content:'';
           position:absolute;
           bottom:0;
           width:5rem;
           height:.2rem;
           background-color: var(--background-light-color-2);
           border-radius:15px;
           left:0;
       }
       &::after{
           content:'';
           position:absolute;
           bottom:0;
           width:2.5rem;
           height:.2rem;
           background-color: var(--background-light-color-2);
           border-radius:15px;
           left:0;
       }
       span{
           font-weight:900;
           color:rgba(25,29,43,.44);
           font-size:3rem;
           position:absolute;
           left:0;
           top:20%;
           z-index:-1;
       }
   }
`;
export default Title
