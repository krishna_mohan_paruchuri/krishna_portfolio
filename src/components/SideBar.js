import React from 'react'
import styled from 'styled-components'
import Navigation from './Navigation'

const SideBar = () => {
    return (
        <div>
            <SidebarStyled>
                <Navigation />
            </SidebarStyled>
        </div>
    )
}

const SidebarStyled = styled.div`
width:14rem;
position:fixed;

height:100vh;
background-color:var(--sidebar-dark-color)
`;

export default SideBar;
