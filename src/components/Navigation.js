import styled from 'styled-components'
import React from 'react'
import { NavLink } from 'react-router-dom'
import avatar from '../images/personal.png'

const Navigation = () => {
    return (
        <NavigationStyled>
            <div className="avatar">
                <img src={avatar} alt="personal" />
            </div>
            <ul className="nav-items">
                <li className="nav-item">
                    <NavLink to="/" activeclassName="active-class" exact >Home</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink to="/about" activeclassName="active-class" exact >About</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink to="/skills" activeclassName="active-class" exact >Skills</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink to="/experience" activeclassName="active-class" exact >Experience</NavLink>
                </li>
                <li className="nav-item">
                    <NavLink to="/academics" activeclassName="active-class" exact >Academics</NavLink>
                </li>
                <li classame="nav-item">
                    <NavLink to="/contact" activeclassName="active-class" exact >Contact</NavLink>
                </li>
            </ul>
            <footer className="footer">
                <p>
                    @2021 krishna mohan Portfolio
                </p>
            </footer>


        </NavigationStyled>
    )
}

const NavigationStyled = styled.nav`
display:flex;
justify-content:space-between;
flex-direction:column;
align-items:center;
height:100%;
width:100%;
border-right: 1px solid var(--border-color);
.avatar{
    width:100%;
    border-bottom : 1px solid var(--border-color);
    text-align:center;
    padding:0.5rem 0;
    img{
        
        width:10rem;
        height:10rem;
        border-radius:50%;
        border: 8px solid var(--border-color)
        
    }
}
.nav-items{
    width:100%;
    text-align:center;
    .active-class{
                background-color: var(--primary-color-light);
                color:white;
            }
    li{
        display:block;
        a{
            display:block;
            padding:0.2rem 0;
            position:relative;
            z-index:10;
            text-transform:uppercase;
            transition: all .4s ease-in-out;
            font-weight:600;
            letter-spacing:1px;
            
            &:hover{
                cursor: pointer;
                color: var(--white-color);
            }
            &::before{
                content:"";
                position:absolute;
                bottom:0;
                left:0;
                width:0;
                height:50%;
                background-color:var(--primary-color);
                transition: all 0.4s cubic-bezier(1,-0.2,.25,.95);
                z-index:-1;
                opacity:0.21;

            }
         }
         a:hover::before{
             height:100%;
             width:100%;
         }
    }
}
.footer{
    border-top:1px solid var(--border-color);
    width:100%;
    p{
        padding:1rem 0;
        font-size:0.8rem;
        text-align:center;
        display:block;
        text-transform:capitalize;
    }
}

`;

export default Navigation
