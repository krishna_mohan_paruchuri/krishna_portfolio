import React from 'react'
import styled from 'styled-components'

const Primarybutton = ({ title }) => {
    return (
        <PrimaryButtonStyled>
            {title}

        </PrimaryButtonStyled>
    )
}

const PrimaryButtonStyled = styled.a`
  background-color: var(--primary-color);
  padding: .6rem 2rem;
  color:white;
  cursor:pointer;
  display:inline-block;
  font-size:inherit;
  text-transform:uppercase;
  position:relative;
  transition: all .4s ease-in-out;

  &::after{
      content:"";
      position:absolute;
      width:0;
      height:.2rem;
      transition: all .4s ease-in-out;
      left:0;
      bottom:0;
      opacity:.6;

  }

  &:hover::after{
       background-color:var(--white-color);
       width:100%;

  }

`;


export default Primarybutton
