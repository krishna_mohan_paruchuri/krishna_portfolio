import React from 'react'
import styled from 'styled-components'

const ServiceCard = ({ image, title, paragraph }) => {
    return (
        <ServiceCardStyled class="service-container">
            <div className="container">
                <img className="image-service" src={image} alt="" />
                <h4>{title}</h4>
                <p>{paragraph}</p>
            </div>

        </ServiceCardStyled>
    )
}
const ServiceCardStyled = styled.div`
background-color : var(--background-dark-grey);
border-left:1px solid var(--border-color);
border-top:6px solid var(--border-color);
border-right:1px solid var(--border-color);
border-bottom:1px solid var(--border-color);
transition: all .4s ease-in-out;
 &:hover{
    border-top:6px solid var(--primary-color);
    transform:translateY(-4px);
 }
.container{
    padding:1rem;
    h4{
        color: var(--white-color);
        font-size:1.1rem;
        padding: .6rem 0;
        position: relative;
        &::after{
            content:'';
            width:3.6rem;
            left:0;
            bottom:0;
            position:absolute;
            height:2px;
            background-color:var(--border-color);
            border-radius:8px;
        }
    }
    p{
        padding:.6rem 0;
    }
    .image-service{
      height:4rem;
      width:4rem;
    }
}

`;
export default ServiceCard
