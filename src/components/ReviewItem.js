import React from 'react'
import styled from 'styled-components';

const ReviewItem = ({ text }) => {
    return (
        <ReviewItemStyled>
            <p>{text}</p>
        </ReviewItemStyled>
    )
}

const ReviewItemStyled = styled.div`

padding:1rem .8rem;
border-left: 4px solid var(--border-color);
background-color: var(--background-dark-grey);
position:relative;
width:50%;
&:not(:first-child){
    margin-left:1.4rem;
}
&::after{
    content:"";
    position:absolute;
    left:2rem;
    top:100%;
    border-width:.8rem;
    border-style:solid;
    border-color:var(--background-dark-grey) transparent transparent var(--background-dark-grey);;
}
 p{
     padding: .2rem 0;
 }
`;
export default ReviewItem
