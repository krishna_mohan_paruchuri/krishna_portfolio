import React from 'react'
import { InnerLayout } from '../styles/Layout'
import styled from 'styled-components'
import Title from '../components/Title'
import ServiceCard from './ServiceCard'
import webDesign from '../data/webdev.svg'
import responsive from '../data/responsive.svg'
import versionControl from '../data/database.svg'

const ServiceSection = () => {
    return (
        <InnerLayout>
            <ServiceSectionStyled>
                <Title title={'Services'} span={'services'} />
                <div className="services">
                    <ServiceCard image={webDesign} title={'Web Development'} paragraph={'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium krishna'} />
                    <div className="mid-servicecard">
                        <ServiceCard image={responsive} title={'Responsive Design'} paragraph={' totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.'} />
                    </div>
                    <ServiceCard image={versionControl} title={'Data Base System'} paragraph={' totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.'} />
                </div>

            </ServiceSectionStyled>
        </InnerLayout>
    )
}
const ServiceSectionStyled = styled.section`
 .services{
     margin-top:4rem;
     display:flex;
     justify-content :space-between;
     .mid-servicecard{
         margin:0 1rem;
     }
     
     }

`;
export default ServiceSection
