import React from 'react'
import styled from 'styled-components'
import { InnerLayout } from '../styles/Layout'
import Title from './Title'
import ReviewItem from './ReviewItem'

const ReviewsSection = () => {
    return (

        <ReviewsSectionStyled>
            <Title title={'Reviews'} span={'Reviews'} />
            <InnerLayout>
                <div class="reviews">

                    <ReviewItem text={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed cursus ac nisl id venenatis. Vivamus a nibh ac urna dictum scelerisque ac nec.'} />
                    <ReviewItem text={'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed cursus ac nisl id venenatis. Vivamus a nibh ac urna dictum scelerisque ac nec.'} />
                </div>
            </InnerLayout>
        </ReviewsSectionStyled>

    )
}

const ReviewsSectionStyled = styled.section`

.reviews{
    display:flex;
}

`;

export default ReviewsSection
