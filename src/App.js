import React from 'react'
import styled from 'styled-components'
import SideBar from './components/SideBar'
import { Route, Switch } from 'react-router'
import HomePage from './Pages/HomePage';
import AboutPage from './Pages/AboutPage';
import SkillsPage from './Pages/SkillsPage';
import ExperiencePage from './Pages/ExperiencePage';
import AcademicsPage from './Pages/AcademicsPage';
import ContactPage from './Pages/ContactPage';

function App() {
  return (
    <div className="App">
      <SideBar />
      <MainContentStyled>
        <div className="lines">
          <div className="line-1">
          </div>
          <div className="line-2">
          </div>
          <div className="line-3">
          </div>
          <div className="line-4">
          </div>
        </div>
        <Switch>
          <Route path="/" exact>
            <HomePage />
          </Route>
          <Route path="/about" exact>
            <AboutPage />
          </Route>
          <Route path="/skills" exact>
            <SkillsPage />
          </Route>
          <Route path="/experience" exact>
            <ExperiencePage
            />
          </Route>
          <Route path="/academics" exact>
            <AcademicsPage />
          </Route>
          <Route path="/contact" exact>
            <ContactPage />
          </Route>
        </Switch>

      </MainContentStyled>
    </div>
  );
}

const MainContentStyled = styled.main`
position:relative;
margin-left:14rem;
min-height:100vh;
/* background-color:red; */
.lines{
  position:absolute;
  height:100%;
  width:100%;
  display:flex;
  justify-content:space-evenly;
  .line-1,.line-2,.line-3,.line-4{
    width : .1px;
    min-height:100%;
    background-color: var(--border-color);
    z-index:-2;
  }

}


`;
export default App;
